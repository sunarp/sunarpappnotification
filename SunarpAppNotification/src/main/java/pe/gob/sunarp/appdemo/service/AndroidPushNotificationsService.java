/**
 * 
 */
package pe.gob.sunarp.appdemo.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @author jroldand
 *
 */
@Service
public class AndroidPushNotificationsService {

		@Value("${sunarp.firebase.server.key}")
		private String serverKey;
		@Value("${sunarp.firebase.api.url}")
		private String apiUrl;
		@Value("${sunarp.firebase.content.type}")
		private String contentType;
		
		@Async
		public CompletableFuture<String> send(HttpEntity<String> entity) {
			RestTemplate restTemplate = new RestTemplate();
	 
			ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
			interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + serverKey));
			interceptors.add(new HeaderRequestInterceptor("Content-Type", contentType));
			
			restTemplate.setInterceptors(interceptors);
			String firebaseResponse = restTemplate.postForObject(apiUrl, entity, String.class);
	 
			return CompletableFuture.completedFuture(firebaseResponse);
		}
}
