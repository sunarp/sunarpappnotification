/**
 * 
 */
package pe.gob.sunarp.appdemo.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.sunarp.appdemo.bean.PayloadBean;
import pe.gob.sunarp.appdemo.service.AndroidPushNotificationsService;
 


/**
 * @author jroldand
 *
 */
@RestController
public class WebController {
 
	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;
 
	@RequestMapping(value = "/push", consumes= {"application/json"}, method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> notification(HttpServletRequest servletRequest, @RequestBody PayloadBean payloadBean) throws JSONException{
 
		String topic = payloadBean.getTopic();
		String title = payloadBean.getTitle(); //"Sunarp Notification"
		String bodyMessage = payloadBean.getBody(); //"Happy Message!"
		
		
		if(title==null || title.equals("")) {
			title = "Sunarp Notification";
		}else if(title.trim().length()<=2) {
			title = "Sunarp Notification";
		}else {
			boolean alfa = Pattern.matches("\"|^[a-záéíóúñA-ZÑ]+(\\s*[a-záéíóúñA-ZÑ]*)*[a-záéíóúñA-ZÑ]+$|", title);
			if(!alfa) title = "Sunarp Notification"; 
		}
		
		JSONObject body = new JSONObject();
		body.put("to", topic);
		body.put("priority", "high");
 
		JSONObject notification = new JSONObject();
		notification.put("title", title);
		notification.put("body", bodyMessage);
		
		JSONObject data = new JSONObject();
		data.put("Key-1", "JSA Data 1");
		data.put("Key-2", "JSA Data 2");
 
		body.put("notification", notification);
		body.put("data", data);
 
		HttpEntity<String> request = new HttpEntity<>(body.toString());
 
		CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
		CompletableFuture.allOf(pushNotification).join();
 
		try {
			String firebaseResponse = pushNotification.get();
			
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
 
		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/pushNotification", consumes= {"application/json"}, method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<String> pushNotification(@RequestBody String requestBody) throws JSONException{
		
		HttpEntity<String> request = new HttpEntity<>(requestBody);
		 
		CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
		CompletableFuture.allOf(pushNotification).join();
 
		try {
			String firebaseResponse = pushNotification.get();
			
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
	}

}
